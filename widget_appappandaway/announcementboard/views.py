from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView


from .models import Announcement


# Create your views here.
def announcements(request):
    return render(request, 'announcementboard/announcements.html', 
        {'announcements':Announcement.objects.order_by('-pub_datetime')})

class AnnouncementDetailView(DetailView):
    model = Announcement
    template_name = 'announcementboard/announcement-details.html'

class AnnouncementCreateView(CreateView):
    model = Announcement
    fields = 'title', 'body', 'author'
    template_name = 'announcementboard/announcement-add.html'

class AnnouncementUpdateView(UpdateView):
    model = Announcement
    fields = 'title', 'body', 'author'
    template_name = 'announcementboard/announcement-edit.html'


