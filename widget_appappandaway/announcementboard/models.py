from django.db import models

from django.urls import reverse

from django.utils import timezone

from dashboard.models import WidgetUser


class Announcement(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE, related_name="announcements")
    pub_datetime = models.DateTimeField(verbose_name="Posted On", auto_now_add=True) 

    def __str__(self):
        return '{} by {} {} on {}'.format(
            self.title, self.author.first_name, self.author.last_name,
            self.pub_datetime.strftime("%m/%d/%Y %I:%M %p"), self.body
            )

    def getLikeReacts(self):
        try:
            return '{}'.format(Reaction.objects.get(name="Like", announcement=self).tally)
        
        except:
            return '0'
    
    def getLoveReacts(self):
        try:
            return '{}'.format(Reaction.objects.get(name="Love", announcement=self).tally)
        
        except:
            return '0'
    
    def getAngryReacts(self):
        try:
            return '{}'.format(Reaction.objects.get(name="Angry", announcement=self).tally)
        
        except:
            return '0'
    
    def getFormattedDateTime(self):
        return '{}'.format(self.pub_datetime.strftime("%m/%d/%Y %I:%M %p"))
        
    def get_absolute_url(self):
        return reverse('announcements:announcement-details', kwargs={'pk':self.pk})





class Reaction(models.Model):
    reaction_names = [('Like', 'Like'), ('Love', 'Love'), ('Angry', 'Angry')]
    name = models.CharField(choices=reaction_names, max_length=255)
    tally = models.IntegerField(default=0, blank=True, null=True)
    announcement = models.ForeignKey(Announcement, on_delete=models.CASCADE, related_name="reacts")

    def __str__(self):
        return '{}'.format(self.name)

    def get_absolute_url(self):
        return reverse('reaction_detail', args=[str(self.name)])
