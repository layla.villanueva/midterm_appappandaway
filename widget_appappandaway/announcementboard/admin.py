from django.contrib import admin

from .models import Announcement, Reaction


class AnnouncementAdmin(admin.ModelAdmin):
    model = Announcement

    search_fields = ('title', 'author')

    list_display = ('title', 'author', 'pub_datetime', 'body')


class ReactionAdmin(admin.ModelAdmin):
    model = Reaction

    search_fields = ('name', 'announcement')

    list_display = ('announcement', 'name', 'tally')


# Register your models here.
admin.site.register(Announcement, AnnouncementAdmin)
admin.site.register(Reaction, ReactionAdmin)
