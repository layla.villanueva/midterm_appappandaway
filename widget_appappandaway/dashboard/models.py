from django.db import models
from django.urls import reverse


# Create your models here.
class Department(models.Model):
    dept_name = models.CharField(max_length=48, unique=True)
    home_unit = models.CharField(max_length=48)

    def __str__(self):
        return '{}'.format(self.dept_name)

    def get_absolute_url(self):
        return f"{self.pk}"


class WidgetUser(models.Model):
    first_name = models.TextField(max_length=48)
    middle_name = models.TextField(max_length=48)
    last_name = models.TextField(max_length=48)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='users')

    def __str__(self):
        return '{}, {} {}'.format(self.last_name, self.first_name, self.middle_name)

    def get_absolute_url(self):
        return f"{self.pk}"
