- CSCI 40-F
- Group Members:
	Aragoza, Ian Rafael T. (210441)
	Villanueva, Layla Mikel (216288)
- Final Project: Widget v2
- App Assignments:
	Dashboard - Ian
	Announcement Board - Lay
- Date of Submission: 15/05/2023
- None of the code used in the project was written or discussed with people outside of the members. No illicit behaviors were done in the completion of this project.
- References:
- Signatures:
	(sgd) Ian Rafael Aragoza, 04/03/2023
	(sgd) Layla Mikel Villanueva, 04/03/2023